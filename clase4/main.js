const {nativeTheme,globalShortcut,BrowserWindow,shell,app,ipcMain,Menu,dialog,Notification,nativeImage,Tray } = require("electron")

const Store = require('electron-store');

const store = new Store();
 
let modo="dark";
if (store.get("modo"))
    modo = store.get("modo");
    
let win
app.on("ready",()=>{

    const ret = globalShortcut.register('F10', () => {
        console.log("presiono f10")
        if (!win.webContents.isDevToolsOpened()){
            win.maximize()
            win.webContents.openDevTools()
        }else{
            win.unmaximize()
            win.webContents.closeDevTools()
        }
    })

    if (!ret) {
        console.log('registration failed')
      }

    nativeTheme.themeSource = modo
    win = new BrowserWindow({
        icon: "resources/logo.ico",
        show:false,
        frame :false,
        webPreferences:{
            nodeIntegration:true,
            contextIsolation:false
        }
    })
    win.on("ready-to-show",()=>{
        let trayMenu = Menu.buildFromTemplate([
            { label: 'Notificacion simple', click :showSimpleNotification },
            { label: 'Notificacion avanzada', click: showAdvanceNotification },
            { label: 'Abrir en navegador', click: ()=>{
                shell.openExternal("https://cadif1.com/carrera")
            } },
            { label: 'Abrir calculadora', click: ()=>{
                    shell.openPath("calc")
                } 
            },
            {
                label:"Instalar",click:()=>{
                    win.show();
                    let n=0
                    let timer=setInterval(()=>{
                        n+=0.1
                        win.setProgressBar(n)
                        if (n>1){
                            showAdvanceNotification()
                            clearInterval(timer) 
                            
                        }
                    },500)
                }
            },
            { label: 'Modo oscuro', type:"checkbox",checked:modo=="dark",
                click:function(menuitem){
                    
                    if (menuitem.checked)
                        nativeTheme.themeSource = 'dark';
                    else
                        nativeTheme.themeSource = 'light';

                    store.set("modo",nativeTheme.themeSource)
                } 
            },
            { type : "separator"},
            { label: 'Cerrar' ,click: ()=>{
                app.exit()
            } }
          ])
        // si la aplicacion esta empaquetada, es decir, si se esta ejecutando
        // en desarrollo o en produccion
        if (app.isPackaged)
            tray = new Tray("resources\\logo.ico");
        else
            tray = new Tray("logo.ico");

        tray.setContextMenu(trayMenu)
        tray.setToolTip('CadiF1 app....')
        tray.on("click",()=>{
            if (!win.isVisible())
                win.show()
            else
                win.hide()
        })
    })
    win.loadFile("index.html")
})

// para que las notificaciones muestren un titulo de aplicacion
// personalizado y no muestre electron.app.Electron
app.setAppUserModelId("Curso de Electron");

ipcMain.on("notificacion-simple",()=>{
    console.log("notificando simple....")
    showSimpleNotification()
})

ipcMain.on("notificacion-mejorada",()=>{
    console.log("notificando mejorada....")
    showAdvanceNotification()
})

function showAdvanceNotification(){
    if (Notification.isSupported()){
        let n=new Notification({
            title:"Te estoy avisando",
            body:"Se esta terminando el tiempo",
            icon: nativeImage.createFromPath('resources/logo.ico'),
            timeoutType : "never",
            silent :true,
            closeButtonText: "Copiado"
        })
        n.show()
        n.on("close",()=>{
            console.log("se cerro la notificacion")
        })
        n.on("click",()=>{
            console.log("se hizo click en la notificacion")
            win.setProgressBar(0)
        })
        n.on("failed",()=>{
            dialog.showMessageBox(null,{
                message:n.body,
                title:n.title,
                type : "info",
                icon : nativeImage.createFromPath('resources/logo.ico')
            })
            console.log("fallo la generacion de la notificacion")
        })
    }
}
function showSimpleNotification(){
    let n=new Notification({
        title:"Te estoy avisando",
        body:"Se esta terminando el tiempo"
    })
    n.show()
}